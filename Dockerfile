FROM phusion/baseimage:0.9.17
MAINTAINER Mike Gallaher <pragsmike@gmail.com>

RUN add-apt-repository -y ppa:webupd8team/java
RUN apt-get install -y software-properties-common
RUN apt-get update

# Auto-accept the Oracle JDK license
RUN echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections

RUN apt-get install -y oracle-java8-installer

RUN apt-get install -y -q -q wget

RUN wget https://github.com/clojure/clojurescript/releases/download/r1.7.170/cljs.jar

# Share lein
ENV LEIN_HOME=/usr/local/share/lein

# Install lein
RUN wget https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein -O /usr/local/bin/lein
RUN chmod 0755 /usr/local/bin/lein
RUN LEIN_ROOT=true /usr/local/bin/lein version

RUN apt-get install -y -q -q nodejs npm
RUN ln -s /usr/bin/nodejs /usr/local/bin/node

RUN useradd -m app
RUN mkdir /project
WORKDIR /project
ADD . .

RUN npm install source-map-support

RUN chown -R app .

USER app

ENTRYPOINT ["/bin/bash"]
